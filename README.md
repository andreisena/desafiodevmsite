# Teste de frontend Mobile Site

Este desafio foi criado para avaliar os candidatos as vagas de frontend angular para o time Mobile.

## O desafio
Criar uma aplicação, responsiva contendo as seguintes telas

![Menu](./layout/Menu.png)

![Lista de Produtos](./layout/Produtos.png)

![Produto](./layout/Produto.png)

## A aplicação deve

- Utilizar os mocks disponibilizados no Mocky IO para popular as telas do menu, lista de produtos e detalhe de produto.
- Considerar as flags e atributos disponibilizados no response de cada mock a fim de determinar exibição do depatarmento, ordenação, variação de preços e avaliações.
- Ao clicar em COMPRA na página de detalhes do produto os dados do produto em questão devem ser armazenados no `local storage` a fim de simular um carrinho de compras.
- A aplicação deverá ser responsiva

### Sobre o layout

A estrutura apresentada como modelo deve ser considerada apenas com referência! Não é obrigatório seguir a mesma mas vale considerar a estrutura de catalogo, produto e menu de navegação.
Hoje em dia ter uma plataforma responsiva não é mais um diferêncial, é uma realidade.

- O menu principal do cabeçalho é apenas para efeito de layout, não há necessidade de desenvolver outras páginas;
- O layout deve se adaptar e mudar de acordo com o tamanho da tela
  - Testaremos em smartphones, tablets (modos portrait e landscape) e monitores a partir de 1024px até 1900px
- A largura máxima do conteúdo é 1100px

## O que será avaliado

- HTML semântico, limpo e claro
- CSS responsivo, semântico, reutilizável e seguindo boas práticas
- Conhecimento de Javascript orientado a objeto, funcional e/ou reativo
- Utilização correta de git
- Testes unitários
- O código deverá funcionar no Chrome

## Tecnologias que você pode utilizar

Você é livre para utilizar qualquer framework JS que preferir, mas saiba que nós trabalhamos com AngularJS e Type Script

Na parte de CSS, você é livre para utilizar qualquer preprocessador também, mas *não é permitido utilizar nenhum framework CSS*. Nós utilizamos Stylus.

Sobre _task manager_, _bundler_ e similares, fica à vontade, apenas pedimos que você adicione instruções detalhadas sobre como fazer funcionar a aplicação.

## Diferenciais

## Mocky IO

- Departamentos
http://www.mocky.io/v2/5cabacb430000057001031e1

- Produros
http://www.mocky.io/v2/5cabafdc30000068001031f6

- Produto
http://www.mocky.io/v2/5cabb00c30000072001031f7

## Estrutura de diretórios

- Images
Imagens dos produtos 
- Data
Arquivos JSON contendo os dados disponíveis no Mocky IO 
- layout
Imagens da estrutura proposta para o desafio